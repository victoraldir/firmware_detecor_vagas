#include "Ultrasonic.h"
#include <SPI.h>
#include <Ethernet.h>
#include <EEPROM.h>

#define echoPin 5 //Pino 5 recebe o pulso do echo
#define trigPin 6 //Pino 6 envia o pulso para gerar o echo

//Aqui e definido as porta que controlarao o rele para acionamento das lampadas
#define liberadoPin 4
#define ocupadoPin 3

//Pino de alimentaçao do sensor
#define vccPin 7


byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress server(192,168,1,3); // Endereço do servidor
const int port = 9000;

EthernetClient client;

//iniciando a função e passando os pinos
Ultrasonic ultrasonic(trigPin,echoPin);

//Flag para definir o ultimo status detectado pela vaga. Quando esta flag esta como TRUE o ultimo status foi de vaga ocupada
boolean flagUltimoStatus;

//Contante que define o espaço de detecaçao entre o sensor e o obstaculo (em Centimetros)
const int distanciaSensor = 170;

String senha = "123456sgve";

void setup()
{

  
  
  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    setup();
  }
  
  //Verifica se ja existe um ID gravado na EEEMPROM
  if(isNewSerialEEPROM()){
    Serial.println("O sensor e novo e sera cadastrado");
    persistSerialEEPROM();
    
  
  }else{
    Serial.println("O sensor ja tem ID cadastrado");
    Serial.println("Verificando autenticaçao junto ao servidor");
    
    isAuth();
     
  }
  
   pinMode(echoPin, INPUT); // define o pino 13 como entrada (recebe)
   pinMode(trigPin, OUTPUT); // define o pino 12 como saida (envia)
   
   pinMode(liberadoPin, OUTPUT); // define o pino 11 para o LED amarelo
   pinMode(ocupadoPin, OUTPUT); // define o pino 10 para o LED vermelho
   
   pinMode(vccPin, OUTPUT);  // Define o pino 7 como saida
   digitalWrite(vccPin, HIGH); //Configura o pino 7 para nivel logico alto para alimentar o sensor Ultrasonico
    
   //Verifica de a vaga esta preenchida e aciona o sinal luminoso
  if(verificaVaga()){
    flagUltimoStatus = true;
    sinalLuminosoOcupado(true);
    submeteStatusVaga(true);
  }else{
    flagUltimoStatus = false;
    sinalLuminosoOcupado(false);
    submeteStatusVaga(false);
  }
}

void loop()
{
  //Verifica se a vaga esta preenchida e aciona o sinal luminoso e submete a situaço para o servidor
  if(verificaVaga()){
    if(!flagUltimoStatus){
      flagUltimoStatus = true;
      sinalLuminosoOcupado(true);
      submeteStatusVaga(true);
    }
  }else{
    if(flagUltimoStatus){
      flagUltimoStatus = false;
      sinalLuminosoOcupado(false);
      submeteStatusVaga(false);
    }
    
  }
  
  //delay(5000);  

}

//Le os segmentos de memoria 0 e 1 e verifica se existem valores gravados. (USAR o exemplo EEPROM Clean antes de aplicar este Script)
boolean isNewSerialEEPROM(){
  Serial.println("Entrou na verificaçao da EEPROM");
  int endereco_0 = EEPROM.read(0);
  int endereco_1 = EEPROM.read(1);
  
  if(endereco_0 == 0 && endereco_1 == 0){
    Serial.println("Eh novo");
    return true;
  }else{
    return false;  
  }
  
}

//Executa o processo de cadastro do Sensor junto ao servidor e salva o ID na EEPROM
boolean persistSerialEEPROM(){

      String url = "GET /insertNewSensor?pwrd="+ senha +" HTTP/1.0";
      
      String response = requestServer(url);
      char responseChar[8];
      
      response.toCharArray(responseChar, 8);
      
      int valorEmInt = atoi(responseChar);
      
      if(valorEmInt != 0){
        Serial.println("Gravando na EEPROM...");
        Serial.println("O ID redebido foi:");
        Serial.println(valorEmInt);
        int endereco_0;
        int endereco_1;
         
        if(valorEmInt < 256){
          endereco_0 = 0;
          endereco_1 = valorEmInt; 
        }else{
          endereco_0 = valorEmInt/256;
          endereco_1 = valorEmInt - (endereco_0 * 256); 
        }
        
        EEPROM.write(0, endereco_0);
        EEPROM.write(1, endereco_1);
        
        Serial.print("endreco_0 = ");
        Serial.println(endereco_0, DEC);
        Serial.print("endreco_1 = ");
        Serial.println(endereco_1, DEC);
        
        delay(3000);
        confirmaFlagServidor();
        
      }else{        
        Serial.println("Erro ao receber ID");
        delay(2000);
        persistSerialEEPROM();
      }
    
  return true;
}

boolean confirmaFlagServidor(){
  Serial.println("Metodo confirmaFlagServidor()");
    
    String url = "GET /confirmEEPROMPersist?pwrd=" + senha + "&key=" + getKey();
    url += " HTTP/1.0";
    
    Serial.println("URL da consulta: " + url);
    
    String response = requestServer(url);
    
    if(response.equals("true")){
      Serial.println("Sensor esta autenticado!");
      return true;  
    }else{
      Serial.println("Tentando novamente...");
      delay(4000);
      confirmaFlagServidor();
    }  
}

//Verifica se o sensor foi autenticado
boolean isAuth(){
   String url = "GET /verificaAutenticacao?pwrd="+ senha +"&key=" + getKey();
   url += " HTTP/1.0";
    
    Serial.println("URL da consulta: " + url);
    
    String response = requestServer(url);
    
    if(response.equals("true")){
      return true;  
    }else{
     return confirmaFlagServidor();
    }  
}

//Retorno 0 se o sensor ainda no tiver ID gravado na EEPROM
String getKey(){
  int endereco_0 = EEPROM.read(0);
  int endereco_1 = EEPROM.read(1);
  int retorno = 0;
  
  if(!(endereco_0 == 0 && endereco_1 == 0)){
    
    retorno = (endereco_0 * 256) + endereco_1;
  }
  char buf[12];
  
  return itoa(retorno,buf,10);
}

//Retorna TRUE se a vaga estiver ocupada
boolean verificaVaga(){

    int distancia = calculaDistanciaVeiculo();

if(distancia < 100){
  return true;
}else{
  return false;
}

//delay(1000); //espera 1 segundo para fazer a leitura novamente

}

void submeteStatusVaga(boolean param){
    Serial.println("submeteStatusVaga()");
    String url;
    String response;
    
    if(param){
      
      url = "GET /setSensoresVagas?pwrd=" + senha + "&status=true&key=" + getKey();
      url += " HTTP/1.0";
      response = requestServer(url);
      
      if(!response.equals("true")){
        submeteStatusVaga(param);
      }
      
    }else{
      url = "GET /setSensoresVagas?pwrd=" + senha + "&status=false&key=" + getKey();
      url += " HTTP/1.0";
      response = requestServer(url);
      if(!response.equals("true")){
        submeteStatusVaga(param);
      }
    }
}

int calculaDistanciaVeiculo(){
 //seta o pino 12 com um pulso baixo "LOW" ou desligado ou ainda 0
    digitalWrite(trigPin, LOW);
  // delay de 2 microssegundos
    delayMicroseconds(2);
  //seta o pino 12 com pulso alto "HIGH" ou ligado ou ainda 1
    digitalWrite(trigPin, HIGH);
  //delay de 10 microssegundos
    delayMicroseconds(10);
  //seta o pino 12 com pulso baixo novamente
    digitalWrite(trigPin, LOW);
  // função Ranging, faz a conversão do tempo de
  //resposta do echo em centimetros, e armazena
  //na variavel distancia
    int distancia = (ultrasonic.Ranging(CM));
   
   return distancia; 
}

//TRUE para acender a luz de ocupado ou FALSE para acender a disponvel
void sinalLuminosoOcupado(boolean param){  
  if(param){
    digitalWrite(ocupadoPin, HIGH);
    digitalWrite(liberadoPin, LOW);
  }else{
    digitalWrite(ocupadoPin, LOW);
  digitalWrite(liberadoPin, HIGH);
  }  
}

String requestServer(String url){
  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("connecting...");

  // if you get a connection, report back via serial:
  if (client.connect(server, 9000)) {
    Serial.println("connected");
    // Make a HTTP request:
    Serial.print(url);
    client.println(url);
    client.println();
  } 
  else {
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  
  String retorno;
  boolean flagAtivo = false;
  while (client.available()) {
    char c = client.read();
    
    if(c == '{' || flagAtivo){
      if(c == '}'){
        retorno += c;
        flagAtivo = false;
      }else{
      retorno += c;
      flagAtivo = true;
      }
      if(retorno.length() > 15){
        retorno = "";
        break;
      }
    }
  }
  
  retorno.replace("{","");
  retorno.replace("}","");
  
  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();

  }
  
  return retorno;
}
